# logback-demo-log

- **Run ELK stack successfully then run java main class**

## Run ELK stack
````
$ cd elk/logback-demo-log/elk

$ docker-compose up
````

## Open kibana
````
http://localhost:5601
````
